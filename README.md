# @universis/grammar

Grammar utils and converters

## Installation

    npm i @universis/grammar

## Usage

    import {GenitiveFormatter, GenderEnum} from '@universis/grammar';

    const genitive = GenitiveFormatter.format('ΓΕΩΡΓΙΟΣ', 'el', {
        gender: GenderEnum.Male
    }); // ΓΕΩΡΓΙΟΥ
