declare interface GrammarLocale {
    genitive: {
        Male?: {
            replacements: string[][];
        },
        Female?: {
            replacements: string[][];
        }
    }
}

export {
    GrammarLocale
}