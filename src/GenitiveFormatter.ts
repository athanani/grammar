import { GrammarLocale } from './GrammarLocale';
import { GenderEnum } from './GenderEnum';
import * as locales from './locales';

declare interface GenitiveFormatterOptions {
    gender: GenderEnum
}

class GenitiveFormatter {
    static format(value: string, locale: string, options: GenitiveFormatterOptions): string {
        if (value == null) {
            return value;
        }
        if (Object.prototype.hasOwnProperty.call(locales, locale) === false) {
            return value;
        }
        const currentLocale: GrammarLocale = locales[locale];
        let replacements: string[][] = [];
        if (options.gender === GenderEnum.Male && currentLocale.genitive.Male != null) {
            replacements = currentLocale.genitive.Male.replacements;
        } else if (options.gender === GenderEnum.Female && currentLocale.genitive.Female != null) {
            replacements = currentLocale.genitive.Female.replacements;
        }
        if (replacements.length === 0) {
            return value;
        }
        for(const replacement of replacements) {
            const re = new RegExp(replacement[0], 'g');
            if (re.test(value)) {
                return value.replace(re, replacement[1]);
            }
        }
        return value;
    }
    static registerLocale(locale: string, options: GrammarLocale) {
        Object.defineProperty(locales, locale, {
            configurable: true,
            enumerable: true,
            writable: true,
            value: options
        });
    }
}

export {
    GenitiveFormatterOptions,
    GenitiveFormatter
}